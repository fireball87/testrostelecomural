CREATE TABLE `weather` (
  `id` int(11) NOT NULL,
  `weatherDate` date NOT NULL,
  `temperature` int(11) DEFAULT NULL,
  `windSpeed` int(11) DEFAULT NULL,
  `windDirection` varchar(45) DEFAULT NULL,
  `precipitation` varchar(45) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `atmosphericPressure` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
