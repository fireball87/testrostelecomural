package ru.kalugin.rostelecomural.weather.dao;

import java.util.List;

import ru.kalugin.rostelecomural.weather.genericdao.GenericDao;
import ru.kalugin.rostelecomural.weather.domain.Weather;

public interface WeatherDao extends GenericDao<Weather, Integer>
{
    List<Weather> findByCityName(String cityName);
}
