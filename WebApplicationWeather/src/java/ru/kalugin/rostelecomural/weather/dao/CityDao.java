package ru.kalugin.rostelecomural.weather.dao;

import java.util.List;

import ru.kalugin.rostelecomural.weather.genericdao.GenericDao;
import ru.kalugin.rostelecomural.weather.domain.City;

public interface CityDao extends GenericDao<City, Integer>
{
    List<City> findByCityName(String cityName);
}
