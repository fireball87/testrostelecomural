/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.kalugin.rostelecomural.weather.domain;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Homer
 */
public class Weather implements Serializable{
    
    private Integer id;
    private City    city;
    private Date    weatherDate;
    private Integer temperature;
    private Integer windSpeed;
    private String  windDirection;
    private String  precipitation;
    private Integer atmosphericPressure;

    public Weather(City city, Date weatherDate, Integer temperature, Integer windSpeed, String windDirection, String precipitation, Integer atmosphericPressure) {
        this.city = city;
        this.weatherDate = weatherDate;
        this.temperature = temperature;
        this.windSpeed = windSpeed;
        this.windDirection = windDirection;
        this.precipitation = precipitation;
        this.atmosphericPressure = atmosphericPressure;
    }
    
    // Default constructor needed by Hibernate
    protected Weather() {

    }

    public Integer getId() {
        return id;
    }

    public City getCity() {
        return city;
    }

    public Date getWeatherDate() {
        return weatherDate;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public Integer getWindSpeed() {
        return windSpeed;
    }

    public String getWindDirection() {
        return windDirection;
    }

    public String getPrecipitation() {
        return precipitation;
    }

    public Integer getAtmosphericPressure() {
        return atmosphericPressure;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public void setWeatherDate(Date weatherDate) {
        this.weatherDate = weatherDate;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    public void setWindSpeed(Integer windSpeed) {
        this.windSpeed = windSpeed;
    }

    public void setWindDirection(String windDirection) {
        this.windDirection = windDirection;
    }

    public void setPrecipitation(String precipitation) {
        this.precipitation = precipitation;
    }

    public void setAtmosphericPressure(Integer atmosphericPressure) {
        this.atmosphericPressure = atmosphericPressure;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    
}
