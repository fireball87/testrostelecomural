/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.kalugin.rostelecomural.weather.domain;

import java.io.Serializable;

/**
 *
 * @author Homer
 */
public class City implements Serializable {
    
    private Integer id;
    private String  cityName;
    
    public City(String cityName) {
        this.cityName = cityName;
    }
    
    // Default constructor needed by Hibernate
    protected City() {

    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
   
    public Integer getId() {
        return id;
    }

    public String getCityName() {
        return cityName;
    } 
}
