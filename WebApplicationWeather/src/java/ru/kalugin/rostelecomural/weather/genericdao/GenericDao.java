/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.kalugin.rostelecomural.weather.genericdao;

import java.io.Serializable;

/**
 *
 * @author Homer
 * 
 * The basic GenericDao interface with CRUD methods
 * 
 */
public interface GenericDao<T, PK extends Serializable> {
    
    PK create(T newInstance);

    T read(PK id);
    
    void update(T transientObject);

    void delete(T persistentObject);
    
}
