package ru.kalugin.rostelecomural.weather.citytest;

import java.util.List;
import java.util.Iterator;

import ru.kalugin.rostelecomural.weather.dao.CityDao;
import ru.kalugin.rostelecomural.weather.domain.City;
import junit.framework.TestCase;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
//import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate4.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * Simple test of the WeatherDao
 */
public class CityDaoTest extends TestCase
{
    private ApplicationContext factory;

    public CityDaoTest(String s)
    {
        super(s);
        factory = new ClassPathXmlApplicationContext("test-applicationContext.xml");
    }

    public void testCrud() throws Exception
    {

        // Create
        CityDao cityDao = getCityDao();
        City createCity = new City("Ekaterinburg");
        cityDao.create(createCity);
        assertNotNull(createCity.getId());
        Integer id = createCity.getId();

        restartSession();

        // Read
        City foundCity = cityDao.read(id);
        assertEquals(createCity.getCityName(), foundCity.getCityName());

        //restartSession();

        // Update
        //Integer updateWeight = 90;
        //foundPerson.setWeight(updateWeight);
       //personDao.update(foundPerson);
        //Person updatedPerson = personDao.read(id);
        //assertEquals(updateWeight, updatedPerson.getWeight());

        restartSession();

        // Delete
        cityDao.delete(foundCity);
        restartSession();
        assertNull(cityDao.read(id));
    }

    public void testFindByName() throws Exception
    {
        CityDao cityDao = getCityDao();
        City city1 = new City("Ekaterinburg");
        cityDao.create(city1);
        City city2 = new City("Moscow");
        cityDao.create(city2);

        restartSession();

        List<City> byName = cityDao.findByCityName("Ekaterinburg");
        assertTrue(byName.size() == 1);
        assertEquals(city1.getCityName(), byName.get(0).getCityName());

        restartSession();

        cityDao.delete(city1);
        cityDao.delete(city2);
    }

    protected void setUp() throws Exception
    {
        openSession();
    }

    protected void tearDown() throws Exception
    {
        closeSession();
    }

    private void openSession()
    {
        SessionFactory sessionFactory = getSessionFactory();
        Session session = sessionFactory.withOptions().openSession();
        TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(session));
    }

    private void closeSession()
    {
        SessionFactory sessionFactory = getSessionFactory();
        SessionHolder sessionHolder = (SessionHolder) TransactionSynchronizationManager.unbindResource(sessionFactory);
        sessionHolder.getSession().flush();
        sessionHolder.getSession().close();
        //SessionFactoryUtils.releaseSession(sessionHolder.getSession(), sessionFactory);
    }

    private void restartSession()
    {
        closeSession();
        openSession();
    }

    private SessionFactory getSessionFactory()
    {
        return (SessionFactory) factory.getBean("sessionFactory");
    }

    private CityDao getCityDao()
    {
        CityDao cityDao = (CityDao) factory.getBean("cityDao");
        return cityDao;
    }
}
