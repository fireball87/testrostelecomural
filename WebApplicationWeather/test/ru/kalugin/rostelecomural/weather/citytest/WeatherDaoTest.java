package ru.kalugin.rostelecomural.weather.citytest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Iterator;
import java.util.Locale;

import ru.kalugin.rostelecomural.weather.dao.CityDao;
import ru.kalugin.rostelecomural.weather.dao.WeatherDao;
import ru.kalugin.rostelecomural.weather.domain.City;
import ru.kalugin.rostelecomural.weather.domain.Weather;
import junit.framework.TestCase;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
//import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate4.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * Simple test of the WeatherDao
 */
public class WeatherDaoTest extends TestCase
{
    private ApplicationContext factory;

    public WeatherDaoTest(String s)
    {
        super(s);
        factory = new ClassPathXmlApplicationContext("test-applicationContext.xml");
    }

    public void testCrud2() throws Exception
    {

        // Create
        CityDao cityDao = getCityDao();
        City createCity = new City("Ekaterinburg");
        cityDao.create(createCity);
        
        String string = "2016-07-21";
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date = format.parse(string);
        Integer temperature = new Integer(25);
        Integer windSpeed = new Integer(1);
        Integer atmosphericPressure = new Integer(739);
        WeatherDao weatherDao = getWeatherDao();      
        Weather createWeather = new Weather(createCity, date, temperature, windSpeed, "east", "clear", atmosphericPressure);
        weatherDao.create(createWeather);
        assertNotNull(createWeather.getId());
        Integer id = createWeather.getId();    
        
        restartSession();

        // Read
        Weather foundWeather = weatherDao.read(id);
        assertEquals(foundWeather.getTemperature(), createWeather.getTemperature());

        restartSession();

        // Update
        temperature = new Integer(30);
        foundWeather.setTemperature(temperature);
        weatherDao.update(foundWeather);
        Weather updatedWeather = weatherDao.read(id);
        assertEquals(temperature, updatedWeather.getTemperature());

        restartSession();

        // Delete
        cityDao.delete(createCity);
        weatherDao.delete(foundWeather);
        restartSession();
        assertNull(weatherDao.read(id));
    }

    public void testFindByName() throws Exception
    {
        CityDao cityDao = getCityDao();
        City city1 = new City("Ekaterinburg");
        cityDao.create(city1);
        City city2 = new City("Moscow");
        cityDao.create(city2);
        
        String string = "2016-07-21";
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date = format.parse(string);
        Integer temperature = new Integer(25);
        Integer windSpeed = new Integer(1);
        Integer atmosphericPressure = new Integer(739);
        WeatherDao weatherDao = getWeatherDao();      
        Weather weather1 = new Weather(city1, date, temperature, windSpeed, "east", "clear", atmosphericPressure);
        Weather weather2 = new Weather(city1, date, temperature, windSpeed, "west", "rain", atmosphericPressure);
        Weather weather3 = new Weather(city2, date, temperature, windSpeed, "south", "snow", atmosphericPressure);
        
        weatherDao.create(weather1);
        weatherDao.create(weather2);
        weatherDao.create(weather3);
        restartSession();

        List<Weather> byName = weatherDao.findByCityName("Ekaterinburg");
        assertTrue(byName.size() == 2);
        assertEquals(city1.getCityName(), byName.get(0).getCity().getCityName());

        restartSession();
        
        cityDao.delete(city1);
        cityDao.delete(city2);
        weatherDao.delete(weather1);
        weatherDao.delete(weather2);
        weatherDao.delete(weather3);
    }

    protected void setUp() throws Exception
    {
        openSession();
    }

    protected void tearDown() throws Exception
    {
        closeSession();
    }

    private void openSession()
    {
        SessionFactory sessionFactory = getSessionFactory();
        Session session = sessionFactory.withOptions().openSession();
        TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(session));
    }

    private void closeSession()
    {
        SessionFactory sessionFactory = getSessionFactory();
        SessionHolder sessionHolder = (SessionHolder) TransactionSynchronizationManager.unbindResource(sessionFactory);
        sessionHolder.getSession().flush();
        sessionHolder.getSession().close();
        //SessionFactoryUtils.releaseSession(sessionHolder.getSession(), sessionFactory);
    }

    private void restartSession()
    {
        closeSession();
        openSession();
    }

    private SessionFactory getSessionFactory()
    {
        return (SessionFactory) factory.getBean("sessionFactory");
    }

    private CityDao getCityDao()
    {
        CityDao cityDao = (CityDao) factory.getBean("cityDao");
        return cityDao;
    }
    
    private WeatherDao getWeatherDao()
    {
        WeatherDao weatherDao = (WeatherDao) factory.getBean("weatherDao");
        return weatherDao;
    }
}
